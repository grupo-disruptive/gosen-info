<!-- Meta UTF8 charset -->
<meta charset="UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="theme-color" content="#000">

<title>Gosen</title>

<link rel="preload" as="style" href="{{ asset('css/fonts-googleapis.css') }}" />
<link rel="stylesheet" href="{{ asset('css/fonts-googleapis.css') }}" />
<noscript>
    <link rel="stylesheet" href="{{ asset('css/fonts-googleapis.css') }}" />
</noscript>

<!-- Este meta indica a los motores de búsqueda que se permita una vista previa grande de las imágenes en los resultados de búsqueda. -->
<meta name='robots' content='max-image-preview:large' />

<!-- <link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//fonts.gstatic.com' />
<link rel='dns-prefetch' href='//f8g8b9p5.rocketcdn.me' /> -->

<link href='https://fonts.gstatic.com' crossorigin rel='preconnect' />

<!-- Hoja de estilos CSS para el tema clásico de Gosen -->
<link rel='stylesheet' id='classic-theme-styles-css' href="{{ asset('css/classic-themes.css') }}" type='text/css'
    media='all' />

<!-- Global styles -->
<link id='global-styles-inline-css' rel="stylesheet" href="{{ asset('css/global-styles.css') }}">

<!-- Hoja de estilos CSS para los iconos Ion -->
<link data-minify="1" rel='stylesheet' id='ion-icon-css' href="{{ asset('css/ion-icons.css') }}" type='text/css'
    media='all' />

<!-- Hoja de estilos CSS para Themify -->
<link data-minify="1" rel='stylesheet' id='themify-css' href="{{ asset('css/themify-stylesheet.css') }}"
    type='text/css' media='all' />

<!-- Hoja de estilos CSS para Font Awesome -->
<link data-minify="1" rel='stylesheet' id='font-awesome-css'
    href='https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/cache/min/1/themes/cryptoland/wp-content/themes/cryptoland/framework/dist/font-awesome/fontawesome.min.css?ver=1671770316'
    type='text/css' media='all' />

<!-- Hoja de estilos CSS personalizada para FlexSlider en Gosen -->
<link data-minify="1" rel='stylesheet' id='cryptoland-custom-flexslider-css'
    href="{{ asset('css/flexslider.css') }}" type='text/css' media='all' />

<!-- Hoja de estilos CSS principal de Gosen -->
<link data-minify="1" rel='stylesheet' id='cryptoland-main-css'
    href="{{ asset('css/themes-cryptoland-css-main3.css') }}" type='text/css' media='all' />

<!-- Hoja de estilos CSS extra de Gosen -->
<link data-minify="1" rel='stylesheet' id='cryptoland-extra-css' href="{{ asset('css/cryptoland-extra.css') }}"
    type='text/css' media='all' />

<!-- Hoja de estilos CSS del tema Gosen para la sección de blog -->
<link data-minify="1" rel='stylesheet' id='cryptoland-theme-style-css' href="{{ asset('css/theme-blog.css') }}"
    type='text/css' media='all' />

<!-- Hoja de estilos CSS para actualizaciones de Gosen -->
<link data-minify="1" rel='stylesheet' id='cryptoland-update-css' href="{{ asset('css/framework-update.css') }}"
    type='text/css' media='all' />

<!-- verificar? -->
<link rel="stylesheet" id='cryptoland-update-inline-css' href="{{ asset('css/cryptoland-update-inline.css') }}">

<!-- Hoja de estilos CSS para el plugin JS Composer en la parte frontal de Gosen -->
<link data-minify="1" rel='stylesheet' id='js_composer_front-css' href="{{ asset('css/css-js-composer.min.css') }}"
    type='text/css' media='all' />

<!-- Script para cargar la biblioteca jQuery en Gosen -->
<script type='text/javascript' src="{{ asset('js/jquery.min.js') }}" id='jquery-core-js' defer></script>

<!-- Script para cargar la biblioteca jQuery Migrate en Gosen -->
<script type='text/javascript' src="{{ asset('js/jquery-migrate.min.js') }}" id='jquery-migrate-js' defer></script>

<!-- enlace de la pagina para la el SEO -->
<link rel="canonical" href="https://ninetheme.com/themes/cryptoland/" />

<style type="text/css">
    .recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important;
    }
</style>

<link rel="icon" href="{{ asset('images/isologo-negro.png') }}" sizes="32x32" />

<link rel="icon" href="{{ asset('images/isologo-negro.png') }}" sizes="192x192" />

<link rel="apple-touch-icon" href="{{ asset('images/isologo-negro.png') }}" />

<meta name="msapplication-TileImage" content="{{ asset('images/isologo-negro.png') }}" />

<style type="text/css" id="wp-custom-css">
    @media (max-width:768px) {
        .wpb-js-composer .vc_general.vc_tta.vc_tta-accordion.vc_tta-color-cryptoland_vc_tta-color-purple .vc_tta-panel-title>a:before {
            right: 8.5px;
        }

        .chart__wrap {
            position: relative;
            margin-bottom: 60px;
        }

        .token__info-list {
            margin-bottom: 60px;
        }

        img.platform__img {
            margin-top: 41px;
        }

        .vc_custom_1538511914519 {
            text-align: center !important;
        }

        .press-carousel .owl-dots {
            margin-top: 65px;
        }

        .wpb_single_image.vc_align_left {
            text-align: center;
        }

        .home3 .scroll-down {
            bottom: -70px;
        }

        .wpb-js-composer .vc_general.vc_tta.vc_tta-accordion.vc_tta-color-cryptoland_vc_tta-color-transparent .vc_tta-panel-title>a:before {
            right: 7px;
        }

        body #hero.hero-fullwidth {
            padding: 63% 15px 42%;
            max-height: 100% !important;
        }

        .c-sidebar-1-widget {
            margin-bottom: 60px;
            padding: 0px !important;
        }
    }

    @media (max-width:575px) {
        .c-blog-3-info {
            padding: 10px 0;
        }

        .c-sidebar-1-widget {
            margin-bottom: 60px;
            padding: 0px !important;
        }
    }

    #hero.first-screen p.breadcrumb {
        font-size: 16px;
        font-weight: 700;
        color: #fff;
    }

    .breadcrumb a {
        color: #fff;
        margin: 0 3px;
    }

    .flexslider .slides>li:before,
    .flex-direction-nav li:before,
    .flexslider ul li:before {
        width: 0px;
    }

    .flexslider .flex-direction-nav li {
        position: inherit;
    }

    #gallery-1 .gallery-item {
        float: left;
        margin-top: 0px;
        text-align: center;
        width: 33%;
    }

    .c-sidebar-1-widget ol li,
    .c-sidebar-1-widget ul li {
        text-transform: capitalize;
    }

    ul#recentcomments li span {
        color: #895faa;
        font-weight: 600;
    }
</style>
<style type="text/css" data-type="vc_custom-css">
    @media (max-width:480px) {
        span.flip-clock-divider {
            display: none;
        }

        .economy__video-btn {
            bottom: -200px;
            left: 100px;
        }
    }
</style>
<style type="text/css" data-type="vc_shortcodes-custom-css">
    .vc_custom_1540090914831 {
        margin-bottom: 60px !important;
        padding-top: 270px !important;
        padding-bottom: 50px !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    .vc_custom_1540090914834 {
        padding-top: 150px !important;
        padding-bottom: 50px !important;
    }

    .vc_custom_1540822755560 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822748316 {
        padding-top: 200px !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822748319 {
        margin-top: -60px !important;
        padding-top: 60px !important;
        padding-bottom: 240px !important;
    }

    .vc_custom_1542275875088 {
        padding-top: 150px !important;
        padding-bottom: 150px !important;
    }

    .vc_custom_1542275875090 {
        padding-top: 100px !important;
        padding-bottom: 100px !important;
    }

    .vc_custom_1542275875090 {
        padding-top: 60px !important;
        padding-bottom: 60px !important;
    }

    .vc_custom_1542275875092 {
        padding-bottom: 0px !important;
    }

    .vc_custom_1540822736916 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822727943 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1541377802548 {
        padding-top: 150px !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    .vc_custom_1541377802552 {
        padding-top: 100px !important;
    }

    .vc_custom_1541377802553 {
        padding-top: 60px !important;
    }

    .vc_custom_1540822717725 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822567958 {
        padding-top: 150px !important;
        padding-bottom: 150px !important;
        background-position: 0 0 !important;
        background-repeat: no-repeat !important;
    }

    .vc_custom_1540822567962 {
        padding-top: 100px !important;
        padding-bottom: 100px !important;
    }

    .vc_custom_1540822567964 {
        padding-top: 60px !important;
        padding-bottom: 60px !important;
    }

    .vc_custom_1540822699639 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822689443 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    .vc_custom_1540822679962 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822668810 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1541312560379 {
        padding-top: 150px !important;
        padding-bottom: 150px !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1541312560382 {
        padding-top: 100px !important;
        padding-bottom: 100px !important;
    }

    .vc_custom_1541312560384 {
        padding-top: 60px !important;
        padding-bottom: 60px !important;
    }

    .vc_custom_1541312634664 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822649856 {
        padding-top: 150px !important;
    }

    .vc_custom_1540822649860 {
        padding-top: 100px !important;
    }

    .vc_custom_1540822649862 {
        padding-top: 60px !important;
    }

    .vc_custom_1540822807336 {
        padding-top: 150px !important;
        padding-bottom: 150px !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822807339 {
        padding-top: 100px !important;
        padding-bottom: 100px !important;
    }

    .vc_custom_1540822807341 {
        padding-top: 60px !important;
        padding-bottom: 60px !important;
    }

    .vc_custom_1540822815998 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822839399 {
        margin-bottom: 70px !important;
        padding-top: 120px !important;
    }

    .vc_custom_1540822839403 {
        padding-top: 70px !important;
    }

    .vc_custom_1540822839405 {
        margin-bottom: 0px !important;
        padding-top: 30px !important;
    }

    .vc_custom_1540822858565 {
        padding-top: 150px !important;
        padding-bottom: 150px !important;
    }

    .vc_custom_1540822858569 {
        padding-top: 100px !important;
        padding-bottom: 100px !important;
    }

    .vc_custom_1540822858571 {
        padding-top: 60px !important;
        padding-bottom: 60px !important;
    }

    .vc_custom_1541318039114 {
        padding-bottom: 70px !important;
        background-position: 0 0 !important;
        background-repeat: no-repeat !important;
    }

    .vc_custom_1541318039117 {
        padding-bottom: 60px !important;
    }

    .vc_custom_1541318039118 {
        padding-bottom: 20px !important;
    }

    .vc_custom_1541318039120 {
        padding-bottom: 20px !important;
    }

    .vc_custom_1540822880721 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1541378001037 {
        padding-bottom: 100px !important;
    }

    .vc_custom_1541378001041 {
        padding-bottom: 50px !important;
    }

    .vc_custom_1541378001043 {
        padding-bottom: 10px !important;
    }

    .vc_custom_1541378001045 {
        padding-bottom: 10px !important;
    }

    .vc_custom_1541378397691 {
        padding-top: 150px !important;
        padding-bottom: 120px !important;
    }

    .vc_custom_1541378397696 {
        padding-top: 100px !important;
        padding-bottom: 70px !important;
    }

    .vc_custom_1541378397698 {
        padding-top: 60px !important;
        padding-bottom: 30px !important;
    }

    .vc_custom_1541378397700 {
        padding-top: 60px !important;
        padding-bottom: 30px !important;
    }

    .vc_custom_1540822965299 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

    .vc_custom_1540822934044 {
        padding-top: 150px !important;
        padding-bottom: 150px !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    .vc_custom_1540822934048 {
        padding-top: 100px !important;
        padding-bottom: 100px !important;
    }

    .vc_custom_1540822934049 {
        padding-top: 60px !important;
        padding-bottom: 60px !important;
    }

    .vc_custom_1540822995896 {
        padding-top: 0px !important;
        padding-bottom: 80px !important;
    }

    .vc_custom_1541311315833 {
        margin-bottom: 0px !important;
    }

    .vc_custom_1541311315839 {
        margin-bottom: 25px !important;
    }

    .vc_custom_1541310856157 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541310856167 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541311717614 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541311717622 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541311250978 {
        margin-bottom: 38px !important;
    }

    .vc_custom_1541311250988 {
        margin-bottom: 25px !important;
    }

    .vc_custom_1539210281356 {
        margin-bottom: 12px !important;
        padding-left: 23px !important;
    }

    .vc_custom_1539210347805 {
        margin-bottom: 12px !important;
        padding-left: 23px !important;
    }

    .vc_custom_1539210623984 {
        margin-bottom: 12px !important;
        padding-left: 23px !important;
    }

    .vc_custom_1542275894911 {
        margin-bottom: 0px !important;
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    .vc_custom_1542275894913 {
        margin-top: 50px !important;
        margin-bottom: 50px !important;
    }

    .vc_custom_1542275894914 {
        margin-bottom: 20px !important;
    }

    .vc_custom_1541311798663 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541311798672 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541311811566 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541311811578 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541311846214 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541311846224 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541311857087 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541311857097 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1539274309047 {
        padding-top: 35px !important;
        padding-bottom: 35px !important;
        background-color: #252d4b !important;
    }

    .vc_custom_1542275958714 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1542275958718 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1542275967529 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1542275967532 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1540093414601 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1540093420346 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1540093845457 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1540093302807 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1540093440257 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541312111507 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541312111516 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541312123254 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541312123264 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541312895383 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541312895392 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541312950314 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541312950324 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1540096276475 {
        background-position: center !important;
        background-repeat: no-repeat !important;
        background-size: cover !important;
    }

    .vc_custom_1541312981479 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541312981489 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541312991578 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541312991587 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1539321476183 {
        margin-bottom: 27px !important;
    }

    .vc_custom_1539316549538 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1539316571544 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541313032537 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541313032547 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541313043433 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541313043443 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541313143037 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541313143047 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541313154478 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541313154488 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541318919536 {
        margin-bottom: 100px !important;
    }

    .vc_custom_1541318919540 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541318941792 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541318941803 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1540019071154 {
        background-image: url(https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-bg.png?id=388) !important;
    }

    .vc_custom_1540019082512 {
        background-image: url(https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-bg.png?id=388) !important;
    }

    .vc_custom_1540019091757 {
        background-image: url(https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-bg.png?id=388) !important;
    }

    .vc_custom_1540019103184 {
        background-image: url(https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-bg.png?id=388) !important;
    }

    .vc_custom_1541379477335 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541379477343 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541318999291 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541318999301 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541319021862 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541319021873 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541319038778 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541319038789 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541319061802 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541319061811 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541319116147 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541319116157 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1541319145954 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541319145963 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541319156451 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541319156461 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1540099245053 {
        margin-bottom: 20px !important;
    }

    .vc_custom_1540104081259 {
        margin-bottom: 0px !important;
    }

    .vc_custom_1540104081262 {
        margin-bottom: 20px !important;
    }

    .vc_custom_1541319326086 {
        margin-bottom: 0px !important;
    }

    .vc_custom_1541319326089 {
        margin-bottom: 20px !important;
    }

    .vc_custom_1541319471360 {
        margin-bottom: 0px !important;
    }

    .vc_custom_1541319471363 {
        margin-bottom: 20px !important;
    }

    .vc_custom_1541319639122 {
        margin-bottom: 0px !important;
        padding-top: 0px !important;
    }

    .vc_custom_1541319639125 {
        margin-bottom: 20px !important;
    }

    .vc_custom_1541319639127 {
        padding-top: 10px !important;
    }

    .vc_custom_1541319380489 {
        margin-bottom: 0px !important;
    }

    .vc_custom_1541319392706 {
        margin-bottom: 0px !important;
    }

    .vc_custom_1541319183512 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1541319183521 {
        margin-bottom: 15px !important;
    }

    .vc_custom_1541319193864 {
        margin-bottom: 60px !important;
    }

    .vc_custom_1541319193875 {
        margin-bottom: 35px !important;
    }

    .vc_custom_1540822206129 {
        margin-top: 27px !important;
    }

    .vc_custom_1540822206132 {
        margin-top: 0px !important;
        margin-bottom: 0px !important;
    }

    .vc_custom_1540103009667 {
        margin-bottom: 0px !important;
    }

    .vc_custom_1540103009670 {
        margin-bottom: 30px !important;
    }

    .vc_custom_1540103138302 {
        margin-bottom: 25px !important;
    }

    .vc_custom_1540105361006 {
        margin-bottom: 20px !important;
    }
</style><noscript>
    <style>
        .wpb_animate_when_almost_visible {
            opacity: 1;
        }
    </style>
</noscript><noscript>
    <style id="rocket-lazyload-nojs-css">
        .rll-youtube-player,
        [data-lazy-src] {
            display: none !important;
        }
    </style>
</noscript>