<!DOCTYPE html>
<html lang="es" class="no-js">

<head>
    {{-- header includes resources --}}
        @include('includes.header-resources')
    {{-- header includes resources --}}
</head>

<!-- BODY START -->

<body
    class="home page-template page-template-custom-page page-template-custom-page-php page page-id-279 nt-shortcode-1.0 home3 header-sticky-on header-topbar-off Ninetheme-Cryptoland Ninetheme-Version-2.3.3 wpb-js-composer js-comp-ver-6.9.0 vc_responsive">

    <header class="header sticky-on ">

        <a class="logo nt-logo" href="{{ route('inicio') }}">

            <div class="logo__img bg-none nt-img-logo">

                <img class="static-logo" src="{{ asset('images/isologo-blanco.png') }}" width="51"
                    height="51" alt="Logo"
                    data-lazy-src="{{ asset('images/isologo-blanco.png') }}"><noscript><img class="static-logo"
                        src="{{ asset('images/isologo-blanco.png') }}" width="51" height="51"
                        alt="Logo"></noscript>

                <img class="sticky-logo" src="{{ asset('images/isologo-blanco.png') }}" width="51"
                    height="51" alt="Logo"
                    data-lazy-src="{{ asset('images/isologo-blanco.png') }}"><noscript><img class="sticky-logo"
                        src="{{ asset('images/isologo-blanco.png') }}" width="51" height="51"
                        alt="Logo"></noscript>

            </div>

            <div class="logo__title nt-text-logo">Gosen</div>

        </a>


        <ul class="menu">
            <li class="menu__item"><a href="#about" title="About" class="menu__link">About</a></li>
            <li class="menu__item"><a href="#services" title="Services" class="menu__link">Services</a></li>
            <li class="menu__item"><a href="#timeline" title="Road Map" class="menu__link">Road Map</a></li>
            <li class="menu__item"><a href="#statistic" title="Statistic" class="menu__link">Statistic</a></li>
            <li class="menu__item"><a href="#token" title="Token" class="menu__link">Token</a></li>
            <li class="menu__item"><a href="#document" title="WhitePapers" class="menu__link">WhitePapers</a></li>
            <li class="menu__item"><a href="#team" title="Team" class="menu__link">Team</a></li>
            <li class="menu__item"><a href="#faq" title="FAQ" class="menu__link">FAQ</a></li>
            <li class="menu__item"><a href="https://ninetheme.com/themes/cryptoland/blog/" title="Blog"
                    class="menu__link">Blog</a></li>
        </ul>

        <div class="header__right">

            <select class="select">
                <option value="">RU</option>
                <option value="">UA</option>
                <option value="">EN</option>
            </select>

            <div class="sign-in-wrap"><a href="#0" target="" class="btn-sign-in">Join Cryptoland ICO</a>
            </div>
        </div>

        <div class="btn-menu">
            <div class="one"></div>
            <div class="two"></div>
            <div class="three"></div>
        </div>
    </header>

    <div class="fixed-menu ">
        <div class="fixed-menu__header">

            <a class="logo logo--color mob-sticky-text-logo nt-logo" href="https://ninetheme.com/themes/cryptoland/">

                <div class="logo__img bg-none nt-img-logo">

                    <img class="static-logo" src="{{ asset('images/isologo-blanco.png') }}" width="51"
                        height="51" alt="Logo"
                        data-lazy-src="{{ asset('images/isologo-blanco.png') }}"><noscript><img class="static-logo"
                            src="{{ asset('images/isologo-blanco.png') }}" width="51" height="51"
                            alt="Logo"></noscript>

                    <img class="sticky-logo" src="{{ asset('images/isologo-blanco.png') }}" width="51"
                        height="51" alt="Logo"
                        data-lazy-src="{{ asset('images/isologo-blanco.png') }}"><noscript><img class="sticky-logo"
                            src="{{ asset('images/isologo-blanco.png') }}" width="51" height="51"
                            alt="Logo"></noscript>

                </div>

                <div class="logo__title nt-text-logo">Gosen</div>

            </a>



            <div class="btn-close">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                    x="0px" y="0px" viewBox="0 0 47.971 47.971"
                    style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve" width="512px"
                    height="512px">
                    <path
                        d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88   c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242   C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879   s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"
                        fill="#006DF0" />
                </svg>
            </div>
        </div>

        <div class="fixed-menu__content">

            <ul class="mob-menu">
                <li class="mob-menu__item"><a href="#about" title="About" class="mob-menu__link">About</a></li>
                <li class="mob-menu__item"><a href="#services" title="Services" class="mob-menu__link">Services</a>
                </li>
                <li class="mob-menu__item"><a href="#timeline" title="Road Map" class="mob-menu__link">Road Map</a>
                </li>
                <li class="mob-menu__item"><a href="#statistic" title="Statistic"
                        class="mob-menu__link">Statistic</a>
                </li>
                <li class="mob-menu__item"><a href="#token" title="Token" class="mob-menu__link">Token</a></li>
                <li class="mob-menu__item"><a href="#document" title="WhitePapers"
                        class="mob-menu__link">WhitePapers</a></li>
                <li class="mob-menu__item"><a href="#team" title="Team" class="mob-menu__link">Team</a></li>
                <li class="mob-menu__item"><a href="#faq" title="FAQ" class="mob-menu__link">FAQ</a></li>
                <li class="mob-menu__item"><a href="https://ninetheme.com/themes/cryptoland/blog/" title="Blog"
                        class="mob-menu__link">Blog</a></li>
            </ul>

            <select class="select mob-on">
                <option value="">RU</option>
                <option value="">UA</option>
                <option value="">EN</option>
            </select>

            <div class="btn-wrap mob-on">
                <a href="#0" class="btn-sign-in">Join Cryptoland ICO</a>
            </div>
        </div>
    </div>
    <!-- Back Top -->
    <div class="c-backtop-1 -js-backtop">
        <i class="c-backtop-1-icon fa fa-angle-up"></i>
    </div>
    <!-- Back Top End -->

    <!-- Site Wrapper -->
    <div class="wrapper">
        <div class="container zindex1">
            <div data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540090914831 overflow_visible zindex1 vc_row-has-fill"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540090914831{padding-top: 150px !important;padding-bottom: 50px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <div class="promo promo3 ">
                                <div class="promo__content" data-aos="fade-right">
                                    <h1>Cryptoland Just Entered <span>the Real World</span></h1>
                                    <p>Spend real fights effective anything extra by leading. Mouthwatering leading how
                                        real formula also locked-in have can mountain thought. Jumbo plus shine sale.
                                    </p>
                                    <div class="timer-wrap">
                                        <div class="timer timer2" id="timer"
                                            data-date="2024, 12, 20, 0, 0, 0, 0"></div>
                                        <div class="timer__titles">
                                            <div>Days</div>
                                            <div>Hours</div>
                                            <div>Minutes</div>
                                            <div>Seconds</div>
                                        </div>
                                    </div>
                                    <div class="promo__btns-wrap"><a class="btn btn--orange btn--medium"
                                            href="#0"><span>Buy Tokens 47% Off</span></a><a
                                            class="btn btn--blue btn--big" href="#0">WhitePappers</a></div>
                                    <div class="payments"><img width="45" height="31"
                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2045%2031'%3E%3C/svg%3E"
                                            alt="visa.png"
                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/visa.png"><noscript><img
                                                width="45" height="31"
                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/visa.png"
                                                alt="visa.png"></noscript><img width="50" height="30"
                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2050%2030'%3E%3C/svg%3E"
                                            alt="mc.png"
                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/mc.png"><noscript><img
                                                width="50" height="30"
                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/mc.png"
                                                alt="mc.png"></noscript><img width="32" height="32"
                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2032%2032'%3E%3C/svg%3E"
                                            alt="bitcoin.png"
                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/bitcoin.png"><noscript><img
                                                width="32" height="32"
                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/bitcoin.png"
                                                alt="bitcoin.png"></noscript><img width="88" height="22"
                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2088%2022'%3E%3C/svg%3E"
                                            alt="paypal.png"
                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/paypal.png"><noscript><img
                                                width="88" height="22"
                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/paypal.png"
                                                alt="paypal.png"></noscript></div>
                                </div><img width="703" height="555"
                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20703%20555'%3E%3C/svg%3E"
                                    class="promo__img" alt="promo-bg.png" data-aos="fade-up"
                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/promo-bg.png"><noscript><img
                                        width="703" height="555"
                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/promo-bg.png"
                                        class="promo__img" alt="promo-bg.png" data-aos="fade-up"></noscript>
                                <div class="scroll-down"><img width="39" height="39"
                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2039%2039'%3E%3C/svg%3E"
                                        alt="scroll-down-1.png" data-aos="fade-up"
                                        data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/scroll-down-1.png"><noscript><img
                                            width="39" height="39"
                                            src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/scroll-down-1.png"
                                            alt="scroll-down-1.png" data-aos="fade-up"></noscript></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="economy_background_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822755560 bg-center-top overflow_visible vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="1600" height="762"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201600%20762'%3E%3C/svg%3E"
                            style=" width:100%; min-width:1680px; height:auto; top:-60px; left:50%;" alt=""
                            class="absolute__bg horizontal-center"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/video-bg.png"><noscript><img
                                width="1600" height="762"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/video-bg.png"
                                style=" width:100%; min-width:1680px; height:auto; top:-60px; left:50%;"
                                alt="" class="absolute__bg horizontal-center"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container">
            <div id="economy" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822748316 bg-center-top overflow_visible vc_row-has-fill"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540822748316{margin-top: -60px !important;padding-top: 60px !important;padding-bottom: 240px !important;}}">
                <div
                    class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-4 vc_col-lg-8 vc_col-md-offset-4 vc_col-md-8 vc_col-xs-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <div class="economy"><a data-jarallax-element="-40"
                                    href="https://www.youtube.com/watch?v=7e90gBu4pas"
                                    class="economy__video-btn video-btn popup-youtube"><img width="25"
                                        height="29"
                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2025%2029'%3E%3C/svg%3E"
                                        alt="video-btn.png"
                                        data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/video-btn.png"><noscript><img
                                            width="25" height="29"
                                            src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/video-btn.png"
                                            alt="video-btn.png"></noscript></a>
                                <div class="economy__block ">
                                    <div class="economy__block-content">
                                        <div
                                            class="section-header section-header--white section-header--tire section-header--small-margin">
                                            <h4>DECENTRALISED ECONOMY</h4>
                                            <h2>A banking platform that <span>enables developer solutions</span></h2>
                                        </div>
                                        <p>Spend real fights effective anything extra by leading. Mouthwatering leading
                                            how real formula also locked-in have can mountain thought. Jumbo plus shine
                                            sale.</p>
                                        <ul>
                                            <li><span>Modular structure </span> enabling easy implementation for
                                                different softwares</li>
                                            <li><span>Advanced payment</span> and processing technologies, fine-tuned
                                                from more than 3 years of development testing</li>
                                            <li><span>Unified AppStore</span> for retail cryptocurrency solutions with a
                                                Crypterium products audience</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div class="container">
            <div id="about" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1542275875088"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1542275875088{padding-top: 100px !important;padding-bottom: 100px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1542275875088{padding-top: 60px !important;padding-bottom: 60px !important;}} @media only screen and (max-width: 576px) {.vc_custom_1542275875088{padding-bottom: 0px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-5 vc_col-md-6 vc_col-xs-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper vc_custom_1541311315833"
                            data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541311315833{margin-bottom: 25px !important;}}">
                            <h4 style="color: #545c79;text-align: left"
                                class="ch_933612 style-tire vc_custom_heading vc_custom_1541310856157"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541310856157{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_933612{}} @media only screen and (max-width: 768px) {.ch_933612{}} @media only screen and (max-width: 576px) {.ch_933612{}}">
                                ABOUT ICO</h4>
                            <h2 style="text-align: left" class="ch_112092 vc_custom_heading"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_112092{}} @media only screen and (max-width: 768px) {.ch_112092{}} @media only screen and (max-width: 576px) {.ch_112092{}}">
                                Cryptoland Theme</h2>
                            <div style="font-size: 40px;color: #ffffff;line-height: 45px;text-align: left"
                                class="ch_382349 f-raleway vc_custom_heading vc_custom_1541311717614"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541311717614{margin-bottom: 30px !important;}} @media only screen and (max-width: 992px) {.ch_382349{}} @media only screen and (max-width: 768px) {.ch_382349{font-size:30px!important; line-height:35px!important;}} @media only screen and (max-width: 576px) {.ch_382349{}}">
                                is the best for your ICO</div>
                            <p style="color: #aab2cd;line-height: 30px;text-align: left"
                                class="ch_471976 f-catamaran vc_custom_heading vc_custom_1541311250978"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541311250978{margin-bottom: 25px !important;}} @media only screen and (max-width: 992px) {.ch_471976{}} @media only screen and (max-width: 768px) {.ch_471976{}} @media only screen and (max-width: 576px) {.ch_471976{}}">
                                Spend real fights effective anything extra by leading. Mouthwatering leading how real
                                formula also locked-in have can mountain thought. Jumbo plus shine sale.</p>
                            <div style="font-size: 18px;color: #aab2cd;line-height: 30px;text-align: left"
                                class="ch_382374 list-marker f-catamaran fw400 vc_custom_heading vc_custom_1539210281356"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_382374{}} @media only screen and (max-width: 768px) {.ch_382374{}} @media only screen and (max-width: 576px) {.ch_382374{}}">
                                Mouthwatering leading how real formula also</div>
                            <div style="font-size: 18px;color: #aab2cd;line-height: 30px;text-align: left"
                                class="ch_312280 list-marker f-catamaran fw400 vc_custom_heading vc_custom_1539210347805"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_312280{}} @media only screen and (max-width: 768px) {.ch_312280{}} @media only screen and (max-width: 576px) {.ch_312280{}}">
                                Locked-in have can mountain thought</div>
                            <div style="font-size: 18px;color: #aab2cd;line-height: 30px;text-align: left"
                                class="ch_251565 list-marker f-catamaran fw400 vc_custom_heading vc_custom_1539210623984"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_251565{}} @media only screen and (max-width: 768px) {.ch_251565{}} @media only screen and (max-width: 576px) {.ch_251565{}}">
                                Locked-in have can mountain thought</div>
                        </div>
                    </div>
                </div>
                <div
                    class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-1 vc_col-lg-6 vc_col-md-6 vc_col-xs-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1542275894911 "
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1542275894911{margin-top: 50px !important;margin-bottom: 50px !important;}} @media only screen and (max-width: 576px) {.vc_custom_1542275894911{margin-bottom: 20px !important;}}">

                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="558"
                                            height="504"
                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20558%20504'%3E%3C/svg%3E"
                                            class="vc_single_image-img attachment-full" alt=""
                                            decoding="async" title="about-img"
                                            data-lazy-srcset="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/about-img.png 558w, https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/about-img-300x271.png 300w"
                                            data-lazy-sizes="(max-width: 558px) 100vw, 558px"
                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/about-img.png" /><noscript><img
                                                width="558" height="504"
                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/about-img.png"
                                                class="vc_single_image-img attachment-full" alt=""
                                                decoding="async" title="about-img"
                                                srcset="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/about-img.png 558w, https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/about-img-300x271.png 300w"
                                                sizes="(max-width: 558px) 100vw, 558px" /></noscript></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="about_parallax_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822736916 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="1600" height="1311"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201600%201311'%3E%3C/svg%3E"
                            style=" width:100vw; min-width:1680px; bottom:-500px; left:0;" data-jarallax-element="40"
                            alt="" class="absolute__bg parallax__img"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/about-bg.png"><noscript><img
                                width="1600" height="1311"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/about-bg.png"
                                style=" width:100vw; min-width:1680px; bottom:-500px; left:0;"
                                data-jarallax-element="40" alt=""
                                class="absolute__bg parallax__img"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div id="services_background_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822727943 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="1600" height="1310"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201600%201310'%3E%3C/svg%3E"
                            style=" width:100%; min-width:1680px; height:auto; top:0; left:50%;" alt=""
                            class="absolute__bg horizontal-center"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/services-bg-1.png"><noscript><img
                                width="1600" height="1310"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/services-bg-1.png"
                                style=" width:100%; min-width:1680px; height:auto; top:0; left:50%;" alt=""
                                class="absolute__bg horizontal-center"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="services" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1541377802548 overflow_visible zindex1 vc_row-has-fill"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1541377802548{padding-top: 100px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1541377802548{padding-top: 60px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="color: #545c79;text-align: center"
                                class="ch_297241 normal vc_custom_heading vc_custom_1541311798663"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541311798663{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_297241{}} @media only screen and (max-width: 768px) {.ch_297241{}} @media only screen and (max-width: 576px) {.ch_297241{}}">
                                AWESOME SERVICES</h4>
                            <h2 style="text-align: center" class="ch_186124 vc_custom_heading vc_custom_1541311811566"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541311811566{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_186124{}} @media only screen and (max-width: 768px) {.ch_186124{}} @media only screen and (max-width: 576px) {.ch_186124{}}">
                                Why it needs?</h2>
                            <div class="section-services">
                                <div class="services__items">
                                    <div class="services__left">
                                        <div data-aos="fade-up" class="service">
                                            <div class="service__bg"
                                                style="background-color: #e85f70; box-shadow: 0 0 51px rgba(232, 95, 112, 0.74); box-shadow: 0 0 51px rgba(232, 95, 112, 0.74);">
                                            </div><img width="62" height="68"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2062%2068'%3E%3C/svg%3E"
                                                alt="service-icon-1-1.svg"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/service-icon-1-1.svg"><noscript><img
                                                    width="62" height="68"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/service-icon-1-1.svg"
                                                    alt="service-icon-1-1.svg"></noscript>
                                            <div class="service__title">Mining Service</div>
                                        </div>
                                        <div data-aos="fade-up" data-aos-delay="200" class="service">
                                            <div class="service__bg"
                                                style="background-color: #fa8936; background-image: linear-gradient(-234deg, #ea9d64 0%, #fa8936 100%); box-shadow: 0 0 51px rgba(250, 137, 54, 0.74);">
                                            </div><img width="65" height="65"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2065%2065'%3E%3C/svg%3E"
                                                alt="service-icon-2-1.svg"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/service-icon-2-1.svg"><noscript><img
                                                    width="65" height="65"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/service-icon-2-1.svg"
                                                    alt="service-icon-2-1.svg"></noscript>
                                            <div class="service__title">Cryptoland App</div>
                                        </div>
                                    </div>
                                    <div class="services__right">
                                        <div data-aos="fade-up" data-aos-delay="400" class="service">
                                            <div class="service__bg"
                                                style="background-image: linear-gradient(-234deg, #6ae472 0%, #4bc253 100%); box-shadow: 0 0 51px rgba(75, 194, 83, 0.74);">
                                            </div><img width="63" height="63"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2063%2063'%3E%3C/svg%3E"
                                                alt="service-icon-3-1.svg"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/service-icon-3-1.svg"><noscript><img
                                                    width="63" height="63"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/service-icon-3-1.svg"
                                                    alt="service-icon-3-1.svg"></noscript>
                                            <div class="service__title">Blockchain</div>
                                        </div>
                                        <div data-aos="fade-up" data-aos-delay="600" class="service">
                                            <div class="service__bg"
                                                style="background-color: #0090d5; background-image: linear-gradient(-234deg, #29aceb 0%, #0090d5 100%); box-shadow: 0 0 51px rgba(0, 144, 213, 0.74);">
                                            </div><img width="63" height="63"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2063%2063'%3E%3C/svg%3E"
                                                alt="service-icon-4-1.svg"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/service-icon-4-1.svg"><noscript><img
                                                    width="63" height="63"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/service-icon-4-1.svg"
                                                    alt="service-icon-4-1.svg"></noscript>
                                            <div class="service__title">Exchange</div>
                                        </div>
                                    </div>
                                </div><img width="669" height="669"
                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20669%20669'%3E%3C/svg%3E"
                                    class="services__bg" alt="services-bg1.png"
                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/services-bg1.png"><noscript><img
                                        width="669" height="669"
                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/services-bg1.png"
                                        class="services__bg" alt="services-bg1.png"></noscript>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="timeline_background_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822717725 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-sm vc_hidden-xs">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="189" height="937"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20189%20937'%3E%3C/svg%3E"
                            style=" top:300px; right:75%; opacity:0.1;" alt="" class="absolute__bg"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/road_map.png"><noscript><img
                                width="189" height="937"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/road_map.png"
                                style=" top:300px; right:75%; opacity:0.1;" alt=""
                                class="absolute__bg"></noscript>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="timeline" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822567958 bg-left-center overflow_visible zindex1 vc_row-has-fill vc_row-o-content-middle vc_row-flex"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540822567958{padding-top: 100px !important;padding-bottom: 100px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1540822567958{padding-top: 60px !important;padding-bottom: 60px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="color: #545c79;text-align: center"
                                class="ch_956933 normal vc_custom_heading vc_custom_1541311846214"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541311846214{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_956933{}} @media only screen and (max-width: 768px) {.ch_956933{}} @media only screen and (max-width: 576px) {.ch_956933{}}">
                                ABOUT ICO</h4>
                            <h2 style="text-align: center" class="ch_977526 vc_custom_heading vc_custom_1541311857087"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541311857087{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_977526{}} @media only screen and (max-width: 768px) {.ch_977526{}} @media only screen and (max-width: 576px) {.ch_977526{}}">
                                Road Map</h2>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <div class="row">
                                <div class="col-lg-6 offset-lg-4 col-sm-8 offset-sm-4">
                                    <div class="road road-2 ">
                                        <div class="road__item">
                                            <div class="road__item-metka"></div>
                                            <div class="road__item-content">
                                                <div class="road__item-title">June 2017</div>
                                                <p>Dolly Varden trout flathead tui chub bigmouth buffalo golden loach
                                                    ghost flathead sauger amur pike, jewel tetra roosterfish mora
                                                    herring Pacific lamprey</p>
                                            </div>
                                        </div>
                                        <div class="road__item">
                                            <div class="road__item-metka"></div>
                                            <div class="road__item-content">
                                                <div class="road__item-title">July 2017</div>
                                                <p>Pirate perch smooth dogfish, flagblenny delta smelt, gopher rockfish
                                                    bramble shark Sevan trout queen triggerfish basslet. Redtooth
                                                    triggerfish prickly shark tarwhine tube-eye Reef triggerfish rohu
                                                    longfin dragonfish</p>
                                            </div>
                                        </div>
                                        <div class="road__item">
                                            <div class="road__item-metka"></div>
                                            <div class="road__item-content">
                                                <div class="road__item-title">December 2017</div>
                                                <p>Pacific argentine. Lined sole masu salmon wolffish cutthroat trout
                                                    mustard eel huchen, sea toad grenadier madtom yellow moray Shingle
                                                    Fish wrymouth giant</p>
                                            </div>
                                        </div>
                                        <div class="road__item">
                                            <div class="road__item-metka"></div>
                                            <div class="road__item-content">
                                                <div class="road__item-title">December 2017</div>
                                                <p>Pacific argentine. Lined sole masu salmon wolffish cutthroat trout
                                                    mustard eel huchen, sea toad grenadier madtom yellow moray Shingle
                                                    Fish wrymouth giant</p>
                                            </div>
                                        </div>
                                        <div class="road__item road__item-active">
                                            <div class="road__item-metka"></div>
                                            <div class="road__item-content">
                                                <div class="road__item-title">January 2018</div>
                                                <p>Walleye silverside American sole rockweed gunnel, handfishyellowtail
                                                    clownfish, rocket danio; blue gourami, ayu gulper eel false trevally
                                                    longjaw mudsucker bonytail chub. Yellow moray french angelfish sand
                                                    stargazer northern squawfish shiner dab mola yellow moray sea
                                                    lamprey torrent catfish sauger blue gourami handfish Sacramento
                                                    blackfish</p>
                                            </div>
                                        </div>
                                        <div class="road__item">
                                            <div class="road__item-metka"></div>
                                            <div class="road__item-content">
                                                <div class="road__item-title">April 2018</div>
                                                <p>Blue gourami, ayu gulper eel false trevally longjaw mudsucker
                                                    bonytail chub. Yellow moray french angelfish sand stargazer</p>
                                            </div>
                                        </div>
                                        <div class="road__item">
                                            <div class="road__item-metka"></div>
                                            <div class="road__item-content">
                                                <div class="road__item-title">May 2018</div>
                                                <p>Livebearer greeneye barred danio mosquitofish king of herring.
                                                    Sturgeon tenpounder-píntano tiger shark harelip sucker</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="logo_background_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822699639 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="1680" height="172"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201680%20172'%3E%3C/svg%3E"
                            style=" width:100%; min-width:1680px; height:auto; top:-75px; left:50%;" alt=""
                            class="absolute__bg horizontal-center"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partenrs-bg.png"><noscript><img
                                width="1680" height="172"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partenrs-bg.png"
                                style=" width:100%; min-width:1680px; height:auto; top:-75px; left:50%;"
                                alt="" class="absolute__bg horizontal-center"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="logo" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822689443 bg-right-top overflow_visible zindex1 vc_row-has-fill">
                <div
                    class="wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1539274309047 vc_row-has-fill">
                                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-lg-3 vc_col-md-3 vc_col-xs-6"
                                    data-res-css=" @media only screen and (max-width: 576px) {.vc_custom_1542275958714{margin-bottom: 30px !important;}}">
                                    <div class="vc_column-inner vc_custom_1542275958714">
                                        <div class="wpb_wrapper ">
                                            <div
                                                class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540093414601 same_height">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="147" height="54"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20147%2054'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full" alt=""
                                                            decoding="async" title="partners-logo-1"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-1-1.png" /><noscript><img
                                                                width="147" height="54"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-1-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-1" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                            <p style="font-size: 14px;color: #aab2cd;text-align: center"
                                                class="ch_925571 fw900 vc_custom_heading"
                                                data-res-css=" @media only screen and (max-width: 992px) {.ch_925571{}} @media only screen and (max-width: 768px) {.ch_925571{}} @media only screen and (max-width: 576px) {.ch_925571{}}.ch_925571{letter-spacing:1.4px;}">
                                                ESCROW</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-lg-3 vc_col-md-3 vc_col-xs-6"
                                    data-res-css=" @media only screen and (max-width: 576px) {.vc_custom_1542275967529{margin-bottom: 30px !important;}}">
                                    <div class="vc_column-inner vc_custom_1542275967529">
                                        <div class="wpb_wrapper ">
                                            <div
                                                class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540093420346 same_height">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="119" height="65"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20119%2065'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full" alt=""
                                                            decoding="async" title="partners-logo-2"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-2-1.png" /><noscript><img
                                                                width="119" height="65"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-2-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-2" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                            <p style="font-size: 14px;color: #aab2cd;text-align: center"
                                                class="ch_301156 fw900 vc_custom_heading"
                                                data-res-css=" @media only screen and (max-width: 992px) {.ch_301156{}} @media only screen and (max-width: 768px) {.ch_301156{}} @media only screen and (max-width: 576px) {.ch_301156{}}.ch_301156{letter-spacing:1.4px;}">
                                                RISK: LOW</p>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-4 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div
                                                class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540093845457 same_height">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="147" height="35"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20147%2035'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full" alt=""
                                                            decoding="async" title="partners-logo-3"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-3-1.png" /><noscript><img
                                                                width="147" height="35"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-3-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-3" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>

                                            <div
                                                class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540093302807 ">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"
                                                        style="max-width:95px;"><img width="510" height="71"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20510%2071'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full" alt=""
                                                            decoding="async" title="star-gold"
                                                            data-lazy-srcset="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/star-gold.png 510w, https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/star-gold-300x42.png 300w"
                                                            data-lazy-sizes="(max-width: 510px) 100vw, 510px"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/star-gold.png" /><noscript><img
                                                                width="510" height="71"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/star-gold.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async" title="star-gold"
                                                                srcset="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/star-gold.png 510w, https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/star-gold-300x42.png 300w"
                                                                sizes="(max-width: 510px) 100vw, 510px" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div
                                                class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1540093440257 same_height">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="162" height="49"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20162%2049'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full" alt=""
                                                            decoding="async" title="partners-logo-4"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-4-1.png" /><noscript><img
                                                                width="162" height="49"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-4-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-4" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                            <p style="font-size: 14px;color: #aab2cd;text-align: center"
                                                class="ch_970528 fw900 vc_custom_heading"
                                                data-res-css=" @media only screen and (max-width: 992px) {.ch_970528{}} @media only screen and (max-width: 768px) {.ch_970528{}} @media only screen and (max-width: 576px) {.ch_970528{}}.ch_970528{letter-spacing:1.4px;}">
                                                RISK: LOW</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="cases_background_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822679962 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="777" height="888"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20777%20888'%3E%3C/svg%3E"
                            style=" width:auto; height:auto; top:50px; left:55%;" alt=""
                            class="absolute__bg horizontal-center"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-bg.png"><noscript><img
                                width="777" height="888"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-bg.png"
                                style=" width:auto; height:auto; top:50px; left:55%;" alt=""
                                class="absolute__bg horizontal-center"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div id="cases_background_right_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822668810 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-sm vc_hidden-xs">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="448" height="505"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20448%20505'%3E%3C/svg%3E"
                            style=" width:auto; height:auto; top:0; right:0;" alt=""
                            class="absolute__bg lg-off"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-imgs.png"><noscript><img
                                width="448" height="505"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-imgs.png"
                                style=" width:auto; height:auto; top:0; right:0;" alt=""
                                class="absolute__bg lg-off"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="cases" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1541312560379 bg-right-top overflow_visible zindex1 vc_row-has-fill"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1541312560379{padding-top: 100px !important;padding-bottom: 100px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1541312560379{padding-top: 60px !important;padding-bottom: 60px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="color: #545c79;text-align: center"
                                class="ch_367557 normal vc_custom_heading vc_custom_1541312111507"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541312111507{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_367557{}} @media only screen and (max-width: 768px) {.ch_367557{}} @media only screen and (max-width: 576px) {.ch_367557{}}">
                                SOME FACTS</h4>
                            <h2 style="text-align: center" class="ch_574124 vc_custom_heading vc_custom_1541312123254"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541312123254{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_574124{}} @media only screen and (max-width: 768px) {.ch_574124{}} @media only screen and (max-width: 576px) {.ch_574124{}}">
                                Use Cases</h2>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="cases__item" data-aos="fade-right"><img width="136"
                                                    height="136"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20136%20136'%3E%3C/svg%3E"
                                                    class="cases__item-icon" alt="cases-icon-1.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-1.png"><noscript><img
                                                        width="136" height="136"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-1.png"
                                                        class="cases__item-icon" alt="cases-icon-1.png"></noscript>
                                                <div class="cases__item-content">
                                                    <div class="cases__item-title">Cryptoland App</div>
                                                    <P class="cases__item-text">Asiatic glassfish pilchard
                                                        sandburrower,
                                                        orangestriped triggerfish hamlet Molly Miller trunkfish spiny
                                                        dogfish! Jewel tetra frigate mackerel</P>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="cases__item" data-aos="fade-left" data-aos-delay="200"><img
                                                    width="136" height="136"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20136%20136'%3E%3C/svg%3E"
                                                    class="cases__item-icon" alt="cases-icon-2.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-2.png"><noscript><img
                                                        width="136" height="136"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-2.png"
                                                        class="cases__item-icon" alt="cases-icon-2.png"></noscript>
                                                <div class="cases__item-content">
                                                    <div class="cases__item-title">Mining Service</div>
                                                    <P class="cases__item-text">Spend real fights effective anything
                                                        extra by leading. Mouthwatering leading how real formula also
                                                        locked-in have can mountain thought. Jumbo</P>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="cases__item" data-aos="fade-right"><img width="136"
                                                    height="136"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20136%20136'%3E%3C/svg%3E"
                                                    class="cases__item-icon" alt="cases-icon-3.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-3.png"><noscript><img
                                                        width="136" height="136"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-3.png"
                                                        class="cases__item-icon" alt="cases-icon-3.png"></noscript>
                                                <div class="cases__item-content">
                                                    <div class="cases__item-title">Blockchain</div>
                                                    <P class="cases__item-text">Clownfish catfish antenna codlet
                                                        alfonsino squirrelfish deepwater flathead sea lamprey. Bombay
                                                        duck sand goby snake mudhead</P>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="cases__item" data-aos="fade-left" data-aos-delay="200"><img
                                                    width="136" height="136"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20136%20136'%3E%3C/svg%3E"
                                                    class="cases__item-icon" alt="cases-icon-2.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-2.png"><noscript><img
                                                        width="136" height="136"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-2.png"
                                                        class="cases__item-icon" alt="cases-icon-2.png"></noscript>
                                                <div class="cases__item-content">
                                                    <div class="cases__item-title">Exchange</div>
                                                    <P class="cases__item-text">Barbelless catfish pelican gulper
                                                        candlefish thornfishGulf menhaden ribbonbearer riffle dace black
                                                        dragonfish denticle herring</P>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="cases__item" data-aos="fade-right"><img width="136"
                                                    height="136"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20136%20136'%3E%3C/svg%3E"
                                                    class="cases__item-icon" alt="cases-icon-5.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-5.png"><noscript><img
                                                        width="136" height="136"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-5.png"
                                                        class="cases__item-icon" alt="cases-icon-5.png"></noscript>
                                                <div class="cases__item-content">
                                                    <div class="cases__item-title">Cryptoland</div>
                                                    <P class="cases__item-text">Clownfish catfish antenna codlet
                                                        alfonsino squirrelfish deepwater flathead sea lamprey. Bombay
                                                        duck sand goby snake mudhead</P>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="cases__item" data-aos="fade-left" data-aos-delay="200"><img
                                                    width="136" height="136"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20136%20136'%3E%3C/svg%3E"
                                                    class="cases__item-icon" alt="cases-icon-6.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-6.png"><noscript><img
                                                        width="136" height="136"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/cases-icon-6.png"
                                                        class="cases__item-icon" alt="cases-icon-6.png"></noscript>
                                                <div class="cases__item-content">
                                                    <div class="cases__item-title">Cryptoland App</div>
                                                    <P class="cases__item-text">Asiatic glassfish pilchard
                                                        sandburrower,
                                                        orangestriped triggerfish hamlet Molly Miller trunkfish spiny
                                                        dogfish!</P>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-wrapper btn--center"><a href="#0"
                                    class="btn btn-cases  btn--orange btn--big btn--uppercase"><span>Join
                                        ICO</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="statistic_background_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1541312634664 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="1680" height="1376"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201680%201376'%3E%3C/svg%3E"
                            style=" width:100%; min-width:1680px; height:auto; top:-250px; left:50%;" alt=""
                            class="absolute__bg horizontal-center"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/data-bg-space.png"><noscript><img
                                width="1680" height="1376"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/data-bg-space.png"
                                style=" width:100%; min-width:1680px; height:auto; top:-250px; left:50%;"
                                alt="" class="absolute__bg horizontal-center"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div id="statistic" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid overflow_visible zindex1 vc_row-no-padding vc_row-o-content-middle vc_row-flex">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper ">
                        <div class="data" id="stat">
                            <div class="container data__container">
                                <div class="row">
                                    <div class="col"><img width="1147" height="418"
                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201147%20418'%3E%3C/svg%3E"
                                            class="data__img" alt="data-bg.png"
                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/data-bg.png"><noscript><img
                                                width="1147" height="418"
                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/data-bg.png"
                                                class="data__img" alt="data-bg.png"></noscript>
                                        <div class="counter__item counter__item-1">
                                            <div class="counter__item-title">Current elixit price (BTC)</div>
                                            <div class="counter counter__item-value numscroller">0.052646</div>
                                        </div>
                                        <div class="counter__item counter__item-2">
                                            <div class="counter__item-title">Avarage batches used</div>
                                            <div class="counter counter__item-value numscroller">5.658</div>
                                        </div>
                                        <div class="counter__item counter__item-3">
                                            <div class="counter__item-title">Total batches remaining</div>
                                            <div class="counter counter__item-value numscroller "
                                                data-percantage="+">
                                                20.324</div>
                                        </div>
                                        <div class="counter__item counter__item-4">
                                            <div class="counter__item-title">Percentage batches</div>
                                            <div class="counter counter__item-value numscroller">65</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="facts" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822649856 overflow_visible zindex1 vc_row-o-content-middle vc_row-flex"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540822649856{padding-top: 100px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1540822649856{padding-top: 60px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="color: #545c79;text-align: center"
                                class="ch_24096 normal vc_custom_heading vc_custom_1541312895383"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541312895383{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_24096{}} @media only screen and (max-width: 768px) {.ch_24096{}} @media only screen and (max-width: 576px) {.ch_24096{}}">
                                SOME FACTS</h4>
                            <h2 style="text-align: center"
                                class="ch_781837 vc_custom_heading vc_custom_1541312950314"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541312950314{margin-bottom: 30px !important;}} @media only screen and (max-width: 992px) {.ch_781837{}} @media only screen and (max-width: 768px) {.ch_781837{}} @media only screen and (max-width: 576px) {.ch_781837{}}">
                                Smart Contract API</h2>
                            <div class="facts__line">
                                <div class="facts__line-wrapper">
                                    <div class="facts__line-list">
                                        <div class="facts__item"><img width="103" height="103"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20103%20103'%3E%3C/svg%3E"
                                                class="facts__icon" alt="bitcon-round.png"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/bitcon-round.png"><noscript><img
                                                    width="103" height="103"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/bitcon-round.png"
                                                    class="facts__icon" alt="bitcon-round.png"></noscript>
                                            <div class="facts__title">Bitcoin + RSK</div>
                                        </div>
                                        <div class="facts__item"><img width="103" height="103"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20103%20103'%3E%3C/svg%3E"
                                                class="facts__icon" alt="stellar-round.png"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/stellar-round.png"><noscript><img
                                                    width="103" height="103"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/stellar-round.png"
                                                    class="facts__icon" alt="stellar-round.png"></noscript>
                                            <div class="facts__title">Stellar Lumens
                                            </div>
                                        </div>
                                        <div class="facts__item"><img width="103" height="103"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20103%20103'%3E%3C/svg%3E"
                                                class="facts__icon" alt="counterparty-round.png"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/counterparty-round.png"><noscript><img
                                                    width="103" height="103"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/counterparty-round.png"
                                                    class="facts__icon" alt="counterparty-round.png"></noscript>
                                            <div class="facts__title">Counterparty</div>
                                        </div>
                                        <div class="facts__item"><img width="103" height="103"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20103%20103'%3E%3C/svg%3E"
                                                class="facts__icon" alt="lisk.png"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/lisk.png"><noscript><img
                                                    width="103" height="103"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/lisk.png"
                                                    class="facts__icon" alt="lisk.png"></noscript>
                                            <div class="facts__title">Lisk</div>
                                        </div>
                                        <div class="facts__item"><img width="103" height="103"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20103%20103'%3E%3C/svg%3E"
                                                class="facts__icon" alt="eos-round.png"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/eos-round.png"><noscript><img
                                                    width="103" height="103"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/eos-round.png"
                                                    class="facts__icon" alt="eos-round.png"></noscript>
                                            <div class="facts__title">EOS</div>
                                        </div>
                                    </div>
                                </div><img width="1680" height="205"
                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201680%20205'%3E%3C/svg%3E"
                                    class="facts__bg" alt="facts-bg.png"
                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facts-bg.png"><noscript><img
                                        width="1680" height="205"
                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facts-bg.png"
                                        class="facts__bg" alt="facts-bg.png"></noscript>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div class="container zindex1">
            <div id="token" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822807336 bg-left-center overflow_visible zindex1 vc_row-has-fill vc_row-o-content-middle vc_row-flex"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540822807336{padding-top: 100px !important;padding-bottom: 100px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1540822807336{padding-top: 60px !important;padding-bottom: 60px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-6 vc_col-has-fill">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper vc_custom_1540096276475"><img width="744" height="736"
                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20744%20736'%3E%3C/svg%3E"
                                style=" width:auto; height:auto; top:60px; right:10%;" alt=""
                                class="absolute__bg md-off"
                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/token-img.png"><noscript><img
                                    width="744" height="736"
                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/token-img.png"
                                    style=" width:auto; height:auto; top:60px; right:10%;" alt=""
                                    class="absolute__bg md-off"></noscript></div>
                    </div>
                </div>
                <div
                    class="wpb_animate_when_almost_visible wpb_fadeInRight fadeInRight wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="color: #545c79;text-align: left"
                                class="ch_83827 style-tire vc_custom_heading vc_custom_1541312981479"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541312981479{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_83827{}} @media only screen and (max-width: 768px) {.ch_83827{}} @media only screen and (max-width: 576px) {.ch_83827{}}">
                                ABOUT TOKEN</h4>
                            <h2 style="text-align: left" class="ch_808677 vc_custom_heading vc_custom_1541312991578"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541312991578{margin-bottom: 30px !important;}} @media only screen and (max-width: 992px) {.ch_808677{}} @media only screen and (max-width: 768px) {.ch_808677{}} @media only screen and (max-width: 576px) {.ch_808677{}}">
                                Token Sale</h2>
                            <ul class="token__info-list">
                                <li><span>Token name:</span>Cryptoland Token</li>
                                <li><span>Ticker Symbol:</span>Cryptoland</li>
                                <li><span>Currency Symbol Image:</span>Currency Symbol Image</li>
                                <li><span>Starting Price Pre-ICO:</span>Cryptoland for USD 0.08</li>
                                <li><span>Maximum Eroiy produced:</span>Cryptoland for USD 0.12</li>
                                <li><span>Maximum Eroiy for Sale:</span>2 billion (technical limit)</li>
                                <li><span>Fundraising Goal:</span>USD 48 million</li>
                                <li><span>Minimum Purchase:</span>100 Cryptoland</li>
                            </ul>
                            <div style="font-size: 25px;color: #ffffff;line-height: 25px;text-align: left"
                                class="ch_756299 f-raleway fw700 vc_custom_heading vc_custom_1539321476183"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_756299{}} @media only screen and (max-width: 768px) {.ch_756299{}} @media only screen and (max-width: 576px) {.ch_756299{}}">
                                General description</div>
                            <p style="color: #aab2cd;line-height: 30px;text-align: left"
                                class="ch_739923 fw400 vc_custom_heading vc_custom_1539316549538"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_739923{}} @media only screen and (max-width: 768px) {.ch_739923{}} @media only screen and (max-width: 576px) {.ch_739923{}}">
                                Cryptoland will be released on the basis of Ethereum platform and fully comply with
                                ERC20* standard.</p>
                            <p style="color: #aab2cd;line-height: 30px;text-align: left"
                                class="ch_538755 fw400 vc_custom_heading vc_custom_1539316571544"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_538755{}} @media only screen and (max-width: 768px) {.ch_538755{}} @media only screen and (max-width: 576px) {.ch_538755{}}">
                                Support of this standard guarantees the compatibility of the token with third-party
                                services (wallets, exchanges, listings, etc.), and provides easy integration.</p>
                            <div class="btn-wrapper btn--left"><a href="#0"
                                    class="btn btn-cases  btn--orange btn--small btn--uppercase"><span>Buy
                                        Token</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="document_background_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822815998 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="1748" height="832"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201748%20832'%3E%3C/svg%3E"
                            style=" width:100%; min-width:1748px; top:-150px; left:0;" data-jarallax-element="40"
                            data-type="" data-speed="" alt="" class="absolute__bg parallax__img"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/docs-bg.png"><noscript><img
                                width="1748" height="832"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/docs-bg.png"
                                style=" width:100%; min-width:1748px; top:-150px; left:0;"
                                data-jarallax-element="40" data-type="" data-speed="" alt=""
                                class="absolute__bg parallax__img"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="document" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid overflow_visible zindex1 vc_row-o-content-middle vc_row-flex">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="color: #545c79;text-align: left"
                                class="ch_74767 style-tire vc_custom_heading vc_custom_1541313032537"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541313032537{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_74767{}} @media only screen and (max-width: 768px) {.ch_74767{}} @media only screen and (max-width: 576px) {.ch_74767{}}">
                                OUR FILES</h4>
                            <h2 style="text-align: left" class="ch_488995 vc_custom_heading vc_custom_1541313043433"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541313043433{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_488995{}} @media only screen and (max-width: 768px) {.ch_488995{}} @media only screen and (max-width: 576px) {.ch_488995{}}">
                                Documents</h2>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="doc_wrapper" data-aos="fade-up"><a class="doc"
                                                    href="" title="">
                                                    <div class="doc__content"><img width="468" height="468"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20468%20468'%3E%3C/svg%3E"
                                                            alt="pdf.svg"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/pdf.svg"><noscript><img
                                                                width="468" height="468"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/pdf.svg"
                                                                alt="pdf.svg"></noscript>
                                                        <div class="doc__title">Terms &amp; Conditions</div>
                                                    </div>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="doc_wrapper" data-aos="fade-up" data-aos-delay="200"><a
                                                    class="doc" href="" title="">
                                                    <div class="doc__content"><img width="468" height="468"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20468%20468'%3E%3C/svg%3E"
                                                            alt="pdf.svg"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/pdf.svg"><noscript><img
                                                                width="468" height="468"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/pdf.svg"
                                                                alt="pdf.svg"></noscript>
                                                        <div class="doc__title">White Pappers</div>
                                                    </div>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="doc_wrapper" data-aos="fade-up" data-aos-delay="400"><a
                                                    class="doc" href="" title="">
                                                    <div class="doc__content"><img width="468" height="468"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20468%20468'%3E%3C/svg%3E"
                                                            alt="pdf.svg"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/pdf.svg"><noscript><img
                                                                width="468" height="468"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/pdf.svg"
                                                                alt="pdf.svg"></noscript>
                                                        <div class="doc__title">Privacy Policy</div>
                                                    </div>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="doc_wrapper" data-aos="fade-up" data-aos-delay="600"><a
                                                    class="doc" href="" title="">
                                                    <div class="doc__content"><img width="468" height="468"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20468%20468'%3E%3C/svg%3E"
                                                            alt="pdf.svg"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/pdf.svg"><noscript><img
                                                                width="468" height="468"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/pdf.svg"
                                                                alt="pdf.svg"></noscript>
                                                        <div class="doc__title">Business Profile</div>
                                                    </div>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="token_data" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822839399 overflow_visible zindex1 vc_row-no-padding vc_row-o-content-middle vc_row-flex"
            data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540822839399{padding-top: 70px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1540822839399{margin-bottom: 0px !important;padding-top: 30px !important;}}">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper ">
                        <div class="data-token-section">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div
                                            class="section-header section-header--animated section-header--medium-margin section-header--center">
                                            <h4>Our data</h4>
                                            <h2>Token Distribution</h2>
                                            <div class="bg-title">Token Distribution</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row chart__row align-items-center">
                                    <div class="col-lg-6">
                                        <div class="chart"><img width="532" height="528"
                                                src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20532%20528'%3E%3C/svg%3E"
                                                class="chart__bg" alt="chart-bg.png"
                                                data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/chart-bg.png"><noscript><img
                                                    width="532" height="528"
                                                    src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/chart-bg.png"
                                                    class="chart__bg" alt="chart-bg.png"></noscript>
                                            <div class="chart__wrap" data-chart-brd="2"
                                                data-chart-brdclr="#1f2641" data-chart-cutout="50"><canvas
                                                    id="myChart" class="myChart" width="400"
                                                    height="400"></canvas></div>
                                        </div>
                                    </div>
                                    <div data-aos="fade-left" class="col-lg-6 token-data__animated-content">
                                        <div class="chart__title">Allocation of funds</div>
                                        <p class="chart__text">Total token supply - 152,358</p>
                                        <ul class="chart__legend">
                                            <li data-chart-label="Founders and Team" data-chart-color="#ed804e"
                                                data-chart-value="25"><span
                                                    style="width: 25%;background-image: -webkit-gradient(linear,left top,right top,from(#ed8a4c),to(#ea485c));background-image: -webkit-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: -o-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: linear-gradient(to right,#ed8a4c 0,#ea485c 100%);"></span>25%
                                                Founders and Team</li>
                                            <li data-chart-label="Reserved Funding" data-chart-color="#e14251"
                                                data-chart-value="30"><span
                                                    style="width: 30%;background-image: -webkit-gradient(linear,left top,right top,from(#ed8a4c),to(#ea485c));background-image: -webkit-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: -o-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: linear-gradient(to right,#ed8a4c 0,#ea485c 100%);"></span>30%
                                                Reserved Funding</li>
                                            <li data-chart-label="Advisors" data-chart-color="#d047e4"
                                                data-chart-value="20"><span
                                                    style="width: 20%;background-image: -webkit-gradient(linear,left top,right top,from(#ed8a4c),to(#ea485c));background-image: -webkit-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: -o-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: linear-gradient(to right,#ed8a4c 0,#ea485c 100%);"></span>20%
                                                Advisors</li>
                                            <li data-chart-label="Distribute to Community"
                                                data-chart-color="#77075d" data-chart-value="15"><span
                                                    style="width: 15%;background-image: -webkit-gradient(linear,left top,right top,from(#ed8a4c),to(#ea485c));background-image: -webkit-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: -o-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: linear-gradient(to right,#ed8a4c 0,#ea485c 100%);"></span>15%
                                                Distribute to Community</li>
                                            <li data-chart-label="Bounty campaign" data-chart-color="#ff457a"
                                                data-chart-value="10"><span
                                                    style="width: 10%;background-image: -webkit-gradient(linear,left top,right top,from(#ed8a4c),to(#ea485c));background-image: -webkit-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: -o-linear-gradient(left,#ed8a4c 0,#ea485c 100%);background-image: linear-gradient(to right,#ed8a4c 0,#ea485c 100%);"></span>10%
                                                Bounty campaign</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="faq"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822858565 overflow_visible zindex1"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540822858565{padding-top: 100px !important;padding-bottom: 100px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1540822858565{padding-top: 60px !important;padding-bottom: 60px !important;}}">
                <div
                    class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-2 vc_col-lg-8 vc_col-md-offset-2 vc_col-md-8">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="text-align: center"
                                class="ch_248375 normal vc_custom_heading vc_custom_1541313143037"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541313143037{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_248375{}} @media only screen and (max-width: 768px) {.ch_248375{}} @media only screen and (max-width: 576px) {.ch_248375{}}">
                                FAQ</h4>
                            <h2 style="text-align: center"
                                class="ch_386456 vc_custom_heading vc_custom_1541313154478"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541313154478{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_386456{}} @media only screen and (max-width: 768px) {.ch_386456{}} @media only screen and (max-width: 576px) {.ch_386456{}}">
                                Frequency Asked Questions</h2>
                            <div class="vc_tta-container" data-vc-action="collapseAll">
                                <div
                                    class="vc_general vc_tta vc_tta-accordion vc_tta-color-cryptoland_vc_tta-color-transparent vc_tta-style-classic vc_tta-shape-rounded vc_tta-o-shape-group vc_tta-controls-align-left vc_tta-o-no-fill vc_tta-o-all-clickable">
                                    <div class="vc_tta-panels-container">
                                        <div class="vc_tta-panels">
                                            <div class="vc_tta-panel vc_active" id="1539145981313-cd966911-eaaf"
                                                data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4
                                                        class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                        <a href="#1539145981313-cd966911-eaaf" data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Can American citizens take
                                                                part in the crowdsale?</span><i
                                                                class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <p><span>JavaScript is also used in environments that aren’t
                                                                    web-based, such as PDF documents, site-specific
                                                                    browsers, and desktop widgets. Newer and faster
                                                                    JavaScript virtual machines (VMs) and platforms
                                                                    built upon them have also increased the popularity
                                                                    of JavaScript for server-side web applications. On
                                                                    the client side, JavaScript has been traditionally
                                                                    implemented as an interpreted language, but more
                                                                    recent browsers perform just-in-time
                                                                    compilation.</span></p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_tta-panel" id="1539146087214-9fc0c37a-44a3"
                                                data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4
                                                        class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                        <a href="#1539146087214-9fc0c37a-44a3" data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Does the crowdsale comply
                                                                with
                                                                legal regulations?</span><i
                                                                class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <p><span>JavaScript is also used in environments that aren’t
                                                                    web-based, such as PDF documents, site-specific
                                                                    browsers, and desktop widgets. Newer and faster
                                                                    JavaScript virtual machines (VMs) and platforms
                                                                    built upon them have also increased the popularity
                                                                    of JavaScript for server-side web applications. On
                                                                    the client side, JavaScript has been traditionally
                                                                    implemented as an interpreted language, but more
                                                                    recent browsers perform just-in-time
                                                                    compilation.</span></p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_tta-panel" id="1539146086183-7acac331-dc6e"
                                                data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4
                                                        class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                        <a href="#1539146086183-7acac331-dc6e" data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Can I trade SCR at an
                                                                exchange?</span><i
                                                                class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <p><span>JavaScript is also used in environments that aren’t
                                                                    web-based, such as PDF documents, site-specific
                                                                    browsers, and desktop widgets. Newer and faster
                                                                    JavaScript virtual machines (VMs) and platforms
                                                                    built upon them have also increased the popularity
                                                                    of JavaScript for server-side web applications. On
                                                                    the client side, JavaScript has been traditionally
                                                                    implemented as an interpreted language, but more
                                                                    recent browsers perform just-in-time
                                                                    compilation.</span></p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_tta-panel" id="1539146084877-e113fcb1-76cf"
                                                data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4
                                                        class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                        <a href="#1539146084877-e113fcb1-76cf" data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">What is the difference
                                                                between
                                                                Coin tokens and Power tokens?</span><i
                                                                class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <p><span>JavaScript is also used in environments that aren’t
                                                                    web-based, such as PDF documents, site-specific
                                                                    browsers, and desktop widgets. Newer and faster
                                                                    JavaScript virtual machines (VMs) and platforms
                                                                    built upon them have also increased the popularity
                                                                    of JavaScript for server-side web applications. On
                                                                    the client side, JavaScript has been traditionally
                                                                    implemented as an interpreted language, but more
                                                                    recent browsers perform just-in-time
                                                                    compilation.</span></p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_tta-panel" id="1539145981627-8dac303f-37a1"
                                                data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4
                                                        class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                        <a href="#1539145981627-8dac303f-37a1" data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Why is Cryptonet economic
                                                                model sustainable?</span><i
                                                                class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <p><span>JavaScript is also used in environments that aren’t
                                                                    web-based, such as PDF documents, site-specific
                                                                    browsers, and desktop widgets. Newer and faster
                                                                    JavaScript virtual machines (VMs) and platforms
                                                                    built upon them have also increased the popularity
                                                                    of JavaScript for server-side web applications. On
                                                                    the client side, JavaScript has been traditionally
                                                                    implemented as an interpreted language, but more
                                                                    recent browsers perform just-in-time
                                                                    compilation.</span></p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_tta-panel" id="1539157196314-dc5856a4-b5f9"
                                                data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4
                                                        class="vc_tta-panel-title vc_tta-controls-icon-position-right">
                                                        <a href="#1539157196314-dc5856a4-b5f9" data-vc-accordion
                                                            data-vc-container=".vc_tta-container"><span
                                                                class="vc_tta-title-text">Can I mine SCR?</span><i
                                                                class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="wpb_text_column wpb_content_element ">
                                                        <div class="wpb_wrapper">
                                                            <p><span>JavaScript is also used in environments that aren’t
                                                                    web-based, such as PDF documents, site-specific
                                                                    browsers, and desktop widgets. Newer and faster
                                                                    JavaScript virtual machines (VMs) and platforms
                                                                    built upon them have also increased the popularity
                                                                    of JavaScript for server-side web applications. On
                                                                    the client side, JavaScript has been traditionally
                                                                    implemented as an interpreted language, but more
                                                                    recent browsers perform just-in-time
                                                                    compilation.</span></p>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="advisors" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1541318039114 bg-left-bottom vc_row-has-fill"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1541318039114{padding-bottom: 60px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1541318039114{padding-bottom: 20px !important;}} @media only screen and (max-width: 576px) {.vc_custom_1541318039114{padding-bottom: 20px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1541318919536"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541318919536{margin-bottom: 35px !important;}}">
                                <div class="wpb_column vc_column_container vc_col-sm-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <h4 style="text-align: center"
                                                class="ch_620294 vc_custom_heading vc_custom_1541318941792"
                                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541318941792{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_620294{}} @media only screen and (max-width: 768px) {.ch_620294{}} @media only screen and (max-width: 576px) {.ch_620294{}}">
                                                Family</h4>
                                            <h2 style="text-align: center" class="ch_574996 vc_custom_heading"
                                                data-res-css=" @media only screen and (max-width: 992px) {.ch_574996{}} @media only screen and (max-width: 768px) {.ch_574996{}} @media only screen and (max-width: 576px) {.ch_574996{}}">
                                                Advisors</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="advisor" data-aos="fade-right"><a href=""
                                                    title="" class="advisor__img">
                                                    <div class="advisor__bg  vc_custom_1540019071154"></div><img
                                                        width="237" height="234"
                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20237%20234'%3E%3C/svg%3E"
                                                        class="advisor__img" alt="advisor-avatar-1-1.jpg"
                                                        data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-1-1.jpg"><noscript><img
                                                            width="237" height="234"
                                                            src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-1-1.jpg"
                                                            class="advisor__img"
                                                            alt="advisor-avatar-1-1.jpg"></noscript>
                                                    <div class="advisor__sn"><img width="112" height="112"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                            alt="facebook.svg"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                width="112" height="112"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                alt="facebook.svg"></noscript></div>
                                                </a>
                                                <div class="advisor__content">
                                                    <div class="advisor__title">David Drake</div>
                                                    <div class="advisor__post">CEO Capital Limited</div>
                                                    <p class="advisor__text">JavaScript virtual machines (VMs) and
                                                        platforms built upon them have also increased the popularity of
                                                        JavaScript for server-side web</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="advisor" data-aos="fade-left" data-aos-delay="200"><a
                                                    href="" title="" class="advisor__img">
                                                    <div class="advisor__bg  vc_custom_1540019082512"></div><img
                                                        width="237" height="234"
                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20237%20234'%3E%3C/svg%3E"
                                                        class="advisor__img" alt="advisor-avatar-4-1.jpg"
                                                        data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-4-1.jpg"><noscript><img
                                                            width="237" height="234"
                                                            src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-4-1.jpg"
                                                            class="advisor__img"
                                                            alt="advisor-avatar-4-1.jpg"></noscript>
                                                    <div class="advisor__sn"><img width="112" height="112"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                            alt="linkedin.svg"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                width="112" height="112"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                alt="linkedin.svg"></noscript></div>
                                                </a>
                                                <div class="advisor__content">
                                                    <div class="advisor__title">Ann Balock</div>
                                                    <div class="advisor__post">Cryptonet Speaker</div>
                                                    <p class="advisor__text">JavaScript virtual machines (VMs) and
                                                        platforms built upon them have also increased the popularity of
                                                        JavaScript for server-side web</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="advisor" data-aos="fade-right"><a href=""
                                                    title="" class="advisor__img">
                                                    <div class="advisor__bg  vc_custom_1540019091757"></div><img
                                                        width="237" height="234"
                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20237%20234'%3E%3C/svg%3E"
                                                        class="advisor__img" alt="advisor-avatar-3-1.jpg"
                                                        data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-3-1.jpg"><noscript><img
                                                            width="237" height="234"
                                                            src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-3-1.jpg"
                                                            class="advisor__img"
                                                            alt="advisor-avatar-3-1.jpg"></noscript>
                                                    <div class="advisor__sn"><img width="112" height="112"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                            alt="google-plus.svg"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                width="112" height="112"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                alt="google-plus.svg"></noscript></div>
                                                </a>
                                                <div class="advisor__content">
                                                    <div class="advisor__title">Vladimir Nikitin</div>
                                                    <div class="advisor__post">Cryptonet Team Lead</div>
                                                    <p class="advisor__text">Giant wels roach spotted danio Black
                                                        swallower cowfish bigscale flagblenny central mudminnow.
                                                        Lighthousefish combtooth blenny</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="advisor" data-aos="fade-left" data-aos-delay="200"><a
                                                    href="" title="" class="advisor__img">
                                                    <div class="advisor__bg  vc_custom_1540019103184"></div><img
                                                        width="237" height="234"
                                                        src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20237%20234'%3E%3C/svg%3E"
                                                        class="advisor__img" alt="advisor-avatar-1-1.jpg"
                                                        data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-1-1.jpg"><noscript><img
                                                            width="237" height="234"
                                                            src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/advisor-avatar-1-1.jpg"
                                                            class="advisor__img"
                                                            alt="advisor-avatar-1-1.jpg"></noscript>
                                                    <div class="advisor__sn"><img width="112" height="112"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                            alt="facebook.svg"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                width="112" height="112"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                alt="facebook.svg"></noscript></div>
                                                </a>
                                                <div class="advisor__content">
                                                    <div class="advisor__title">Sam Peters</div>
                                                    <div class="advisor__post">Team Lead Advisor</div>
                                                    <p class="advisor__text">Lampfish combfish, roundhead lemon sole
                                                        armoured catfish saw shark northern stargazer smooth dogfish cod
                                                        icefish scythe butterfish</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="team_parallax_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822880721 bg-center-top overflow_visible zindex-1 vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="1688" height="597"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%201688%20597'%3E%3C/svg%3E"
                            style=" width:100%; min-width:1680px; top:180px; left:0;" data-jarallax-element="40"
                            data-type="" data-speed="" alt="" class="absolute__bg parallax__img"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-bg.png"><noscript><img
                                width="1688" height="597"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-bg.png"
                                style=" width:100%; min-width:1680px; top:180px; left:0;" data-jarallax-element="40"
                                data-type="" data-speed="" alt=""
                                class="absolute__bg parallax__img"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="team" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1541378001037 overflow_visible zindex1"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1541378001037{padding-bottom: 50px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1541378001037{padding-bottom: 10px !important;}} @media only screen and (max-width: 576px) {.vc_custom_1541378001037{padding-bottom: 10px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="text-align: left"
                                class="ch_738783 style-tire md-style-normal vc_custom_heading vc_custom_1541379477335"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541379477335{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_738783{text-align:center!important;}} @media only screen and (max-width: 768px) {.ch_738783{}} @media only screen and (max-width: 576px) {.ch_738783{}}">
                                OUR BRAIN</h4>
                            <h2 style="color: #ffffff;text-align: left"
                                class="ch_847713 vc_custom_heading vc_custom_1541318999291"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541318999291{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_847713{text-align:center!important;}} @media only screen and (max-width: 768px) {.ch_847713{}} @media only screen and (max-width: 576px) {.ch_847713{}}">
                                Awesome Team</h2>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"><img
                                                    width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-1.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-1.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-1.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-1.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">David Drake</div>
                                                    <div class="team-member__post">UI Designer</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="100"><img width="97" height="97"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2097%2097'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-2.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-2.png"><noscript><img
                                                        width="97" height="97"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-2.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-2.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Allan Bellor</div>
                                                    <div class="team-member__post">Analitics</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="200"><img width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-3.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-3.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-3.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-3.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Joe Doe</div>
                                                    <div class="team-member__post">Tech Operation</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="300"><img width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-4.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-4.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-4.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-4.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Sam Tolder</div>
                                                    <div class="team-member__post">CEO</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"><img
                                                    width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-5.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-5.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-5.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-5.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Henry Polar</div>
                                                    <div class="team-member__post">SEO Specialist</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="100"><img width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-8.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-8.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-8.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-8.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Sandra Pen</div>
                                                    <div class="team-member__post">Humar Resources</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="200"><img width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-1.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-1.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-1.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-1.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Linda Gampton</div>
                                                    <div class="team-member__post">UX Team Lead</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="300"><img width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-10.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-10.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-10.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-10.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">John Smith</div>
                                                    <div class="team-member__post">General Director</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"><img
                                                    width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-11.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-11.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-11.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-11.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Sam Oldrich</div>
                                                    <div class="team-member__post">Manager</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="100"><img width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-12.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-12.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-12.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-12.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Denis Portlen</div>
                                                    <div class="team-member__post">Programmer</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="200"><img width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-7.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-7.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-7.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-7.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Den Miller</div>
                                                    <div class="team-member__post">Economist</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="team-member-style-2 team-member " data-aos="fade-right"
                                                data-aos-delay="300"><img width="109" height="109"
                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20109%20109'%3E%3C/svg%3E"
                                                    class="team-member__avatar" alt="team-member-ava-8.png"
                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-8.png"><noscript><img
                                                        width="109" height="109"
                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/team-member-ava-8.png"
                                                        class="team-member__avatar"
                                                        alt="team-member-ava-8.png"></noscript>
                                                <div class="team-member__content">
                                                    <div class="team-member__name">Brawn Lee</div>
                                                    <div class="team-member__post">Journalist</div>
                                                    <ul class="team-member__social">
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="facebook.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/facebook.svg"
                                                                        alt="facebook.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="linkedin.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/linkedin.svg"
                                                                        alt="linkedin.svg"></noscript></a></li>
                                                        <li><a href="" title=""><img width="112"
                                                                    height="112"
                                                                    src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20112%20112'%3E%3C/svg%3E"
                                                                    alt="google-plus.svg"
                                                                    data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"><noscript><img
                                                                        width="112" height="112"
                                                                        src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/google-plus.svg"
                                                                        alt="google-plus.svg"></noscript></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div class="container zindex1">
            <div id="news" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid bg-left-center md-bg-left-center sm-bg-left-center overflow_visible zindex1">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="text-align: center"
                                class="ch_632789 vc_custom_heading vc_custom_1541319021862"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541319021862{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_632789{text-align:center!important;}} @media only screen and (max-width: 768px) {.ch_632789{text-align:center!important;}} @media only screen and (max-width: 576px) {.ch_632789{}}">
                                IN THE WORLD</h4>
                            <h2 style="text-align: center"
                                class="ch_379355 vc_custom_heading vc_custom_1541319038778"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541319038778{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_379355{text-align:center!important;}} @media only screen and (max-width: 768px) {.ch_379355{text-align:center!important;}} @media only screen and (max-width: 576px) {.ch_379355{}}">
                                Latest News</h2>
                            <div class="news-carousel owl-carousel" data-autoplay="yes" data-loop="yes"
                                data-post-per-page="6"><a
                                    href="https://ninetheme.com/themes/cryptoland/2018/10/23/start-the-redemption-before-the-ico-completion-2/"
                                    class="news-carousel__item">
                                    <div class="news-carousel__item-body">
                                        <div class="news-carousel__item-subtitle">Bitcoin / Blockchain</div>
                                        <h3 class="news-carousel__item-title">Start the redemption before the ICO
                                            completion</h3>
                                        <p>Start the redemption before the ICO completion Specially for our VIP
                                            customers the LH Crypto team representatives Alexander Smirnov and Antonis
                                            Lapos will conduct a...</p>
                                        <div class="news-carousel__item-data">October 23, 2018</div>
                                    </div>
                                </a><a
                                    href="https://ninetheme.com/themes/cryptoland/2018/10/23/new-trends-in-ui-ux-design-world-integration/"
                                    class="news-carousel__item">
                                    <div class="news-carousel__item-body">
                                        <div class="news-carousel__item-subtitle">Uncategorized</div>
                                        <h3 class="news-carousel__item-title">New trends in UI/UX Design World
                                            Integration</h3>
                                        <p>The meetings will take place in the following cities: 15 January – Milan, 16
                                            January – Lugano, 17 January – Zurich, 18 January – Geneva,...</p>
                                        <div class="news-carousel__item-data">October 23, 2018</div>
                                    </div>
                                </a><a
                                    href="https://ninetheme.com/themes/cryptoland/2018/10/23/the-crypto-project-has-reached-seven-billions-2/"
                                    class="news-carousel__item">
                                    <div class="news-carousel__item-body">
                                        <div class="news-carousel__item-subtitle">Bitcoin / Blockchain</div>
                                        <h3 class="news-carousel__item-title">The Crypto project has reached seven
                                            billions</h3>
                                        <p>Lh-Crypto project buys 10% of the total amount of LHC tokens sold at a double
                                            rate. Thus, even now, project investors can receive an Wrasse...</p>
                                        <div class="news-carousel__item-data">October 23, 2018</div>
                                    </div>
                                </a><a
                                    href="https://ninetheme.com/themes/cryptoland/2018/10/05/start-the-redemption-before-the-ico-completion/"
                                    class="news-carousel__item">
                                    <div class="news-carousel__item-body">
                                        <div class="news-carousel__item-subtitle">Blokchain / ICO</div>
                                        <h3 class="news-carousel__item-title">Start the redemption before the ICO
                                            completion</h3>
                                        <p>The meetings will take place in the following cities: 15 January – Milan, 16
                                            January – Lugano, 17 January – Zurich, 18 January – Geneva,...</p>
                                        <div class="news-carousel__item-data">October 5, 2018</div>
                                    </div>
                                </a><a
                                    href="https://ninetheme.com/themes/cryptoland/2018/10/05/the-crypto-project-has-reached-seven-billions/"
                                    class="news-carousel__item">
                                    <div class="news-carousel__item-body">
                                        <div class="news-carousel__item-subtitle">Bitcoin / News</div>
                                        <h3 class="news-carousel__item-title">The Crypto project has reached seven
                                            billions</h3>
                                        <p>Specially for our VIP customers the LH Crypto team representatives Alexander
                                            Smirnov and Antonis Lapos will conduct a number of personal meetings. The
                                            meetings will...</p>
                                        <div class="news-carousel__item-data">October 5, 2018</div>
                                    </div>
                                </a><a
                                    href="https://ninetheme.com/themes/cryptoland/2018/09/27/new-trends-crypto-moneys/"
                                    class="news-carousel__item">
                                    <div class="news-carousel__item-body">
                                        <div class="news-carousel__item-subtitle">Bitcoin / Cryptocurrency</div>
                                        <h3 class="news-carousel__item-title">New trends in UI/UX Design World
                                            Integration</h3>
                                        <p>Specially for our VIP customers the LH Crypto team representatives Alexander
                                            Smirnov and Antonis Lapos will conduct a number of personal meetings. The
                                            meetings will...</p>
                                        <div class="news-carousel__item-data">September 27, 2018</div>
                                    </div>
                                </a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div class="container zindex1">
            <div id="press_about" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1541378397691 overflow_visible zindex1 vc_row-o-content-middle vc_row-flex"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1541378397691{padding-top: 100px !important;padding-bottom: 70px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1541378397691{padding-top: 60px !important;padding-bottom: 30px !important;}} @media only screen and (max-width: 576px) {.vc_custom_1541378397691{padding-top: 60px !important;padding-bottom: 30px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="text-align: center"
                                class="ch_248126 vc_custom_heading vc_custom_1541319061802"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541319061802{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_248126{}} @media only screen and (max-width: 768px) {.ch_248126{}} @media only screen and (max-width: 576px) {.ch_248126{}}">
                                PRESS ABOUT US</h4>
                            <h2 style="text-align: center"
                                class="ch_676372 vc_custom_heading vc_custom_1541319116147"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541319116147{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_676372{}} @media only screen and (max-width: 768px) {.ch_676372{}} @media only screen and (max-width: 576px) {.ch_676372{}}">
                                Press About Cryptoland</h2>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="doc_wrapper" data-aos="fade-up"><a class="doc title--none"
                                                    href="" title="">
                                                    <div class="doc__content"><img width="91" height="95"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2091%2095'%3E%3C/svg%3E"
                                                            alt="press-logo-1-1.png"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/press-logo-1-1.png"><noscript><img
                                                                width="91" height="95"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/press-logo-1-1.png"
                                                                alt="press-logo-1-1.png"></noscript></div>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="doc_wrapper" data-aos="fade-up" data-aos-delay="200"><a
                                                    class="doc title--none" href="" title="">
                                                    <div class="doc__content"><img width="83" height="87"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2083%2087'%3E%3C/svg%3E"
                                                            alt="press-logo-2-1.png"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/press-logo-2-1.png"><noscript><img
                                                                width="83" height="87"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/press-logo-2-1.png"
                                                                alt="press-logo-2-1.png"></noscript></div>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="doc_wrapper" data-aos="fade-up" data-aos-delay="400"><a
                                                    class="doc title--none" href="" title="">
                                                    <div class="doc__content"><img width="120" height="87"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20120%2087'%3E%3C/svg%3E"
                                                            alt="press-logo-3.png"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/press-logo-3.png"><noscript><img
                                                                width="120" height="87"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/press-logo-3.png"
                                                                alt="press-logo-3.png"></noscript></div>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="doc_wrapper" data-aos="fade-up" data-aos-delay="600"><a
                                                    class="doc title--none" href="" title="">
                                                    <div class="doc__content"><img width="137" height="98"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20137%2098'%3E%3C/svg%3E"
                                                            alt="press-logo-4.png"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/press-logo-4.png"><noscript><img
                                                                width="137" height="98"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/press-logo-4.png"
                                                                alt="press-logo-4.png"></noscript></div>
                                                </a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div class="container zindex1">
            <div id="partners" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid overflow_visible zindex1">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="text-align: left"
                                class="ch_521020 style-tire md-style-normal xs-style-normal vc_custom_heading vc_custom_1541319145954"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541319145954{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_521020{text-align:center!important;}} @media only screen and (max-width: 768px) {.ch_521020{}} @media only screen and (max-width: 576px) {.ch_521020{}}">
                                OUR FRIENDS</h4>
                            <h2 style="text-align: left" class="ch_133004 vc_custom_heading vc_custom_1541319156451"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541319156451{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_133004{text-align:center!important;}} @media only screen and (max-width: 768px) {.ch_133004{}} @media only screen and (max-width: 576px) {.ch_133004{}}">
                                Partners</h2>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-lg-1/5 vc_col-md-1/5 vc_col-xs-6"
                                    data-res-css=" @media only screen and (max-width: 992px) {.{margin-bottom: 20px !important;}}">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1541319326086 md-center sm-right has_opacity"
                                                data-opacity="0.4" data-hvr-opacity="0.8"
                                                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1541319326086{margin-bottom: 20px !important;}}">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="147" height="54"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20147%2054'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full"
                                                            alt="" decoding="async"
                                                            title="partners-logo-1"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-1-1.png" /><noscript><img
                                                                width="147" height="54"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-1-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-1" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-4 vc_col-lg-1/5 vc_col-md-1/5 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1541319471360 md-center sm-left has_opacity"
                                                data-opacity="0.4" data-hvr-opacity="0.8"
                                                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1541319471360{margin-bottom: 20px !important;}}">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="119" height="65"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20119%2065'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full"
                                                            alt="" decoding="async"
                                                            title="partners-logo-2"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-2-1.png" /><noscript><img
                                                                width="119" height="65"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-2-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-2" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-4 vc_col-lg-1/5 vc_col-md-1/5 vc_col-xs-6">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1541319639122 md-center sm-right has_opacity"
                                                data-opacity="0.4" data-hvr-opacity="0.8"
                                                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1541319639122{margin-bottom: 20px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1541319639122{padding-top: 10px !important;}}">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="147" height="35"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20147%2035'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full"
                                                            alt="" decoding="async"
                                                            title="partners-logo-3"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-3-1.png" /><noscript><img
                                                                width="147" height="35"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-3-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-3" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-1/5 vc_col-md-1/5 vc_col-xs-6"
                                    data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540104081259{margin-bottom: 20px !important;}}">
                                    <div class="vc_column-inner vc_custom_1540104081259">
                                        <div class="wpb_wrapper ">
                                            <div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1541319380489 md-center sm-left has_opacity"
                                                data-opacity="0.4" data-hvr-opacity="0.8">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="162" height="49"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20162%2049'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full"
                                                            alt="" decoding="async"
                                                            title="partners-logo-4"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-4-1.png" /><noscript><img
                                                                width="162" height="49"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-4-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-4" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div
                                    class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-1/5 vc_col-md-1/5 vc_col-xs-12">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper ">
                                            <div class="wpb_single_image wpb_content_element vc_align_left  vc_custom_1541319392706 md-center sm-center has_opacity"
                                                data-opacity="0.4" data-hvr-opacity="0.8">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img
                                                            width="131" height="40"
                                                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20131%2040'%3E%3C/svg%3E"
                                                            class="vc_single_image-img attachment-full"
                                                            alt="" decoding="async"
                                                            title="partners-logo-5"
                                                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-5-1.png" /><noscript><img
                                                                width="131" height="40"
                                                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/partners-logo-5-1.png"
                                                                class="vc_single_image-img attachment-full"
                                                                alt="" decoding="async"
                                                                title="partners-logo-5" /></noscript>
                                                    </div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div id="contact_background_image" data-vc-full-width="true" data-vc-full-width-init="false"
            data-vc-stretch-content="true"
            class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822965299 bg-center-top overflow_visible vc_row-has-fill vc_row-no-padding">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper "><img width="777" height="888"
                            src="data:image/svg+xml,%3Csvg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20777%20888'%3E%3C/svg%3E"
                            style=" width:auto; height:auto; top:0; left:50%;" alt=""
                            class="absolute__bg horizontal-center"
                            data-lazy-src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/subscribe-bg.png"><noscript><img
                                width="777" height="888"
                                src="https://f8g8b9p5.rocketcdn.me/themes/cryptoland/wp-content/uploads/2018/10/subscribe-bg.png"
                                style=" width:auto; height:auto; top:0; left:50%;" alt=""
                                class="absolute__bg horizontal-center"></noscript></div>
                </div>
            </div>
        </div>
        <div class="vc_row-full-width vc_clearfix"></div>
        <div class="container zindex1">
            <div id="contact" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822934044 bg-left-center md-bg-left-center sm-bg-left-center overflow_visible zindex1 vc_row-has-fill vc_row-o-content-middle vc_row-flex"
                data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540822934044{padding-top: 100px !important;padding-bottom: 100px !important;}} @media only screen and (max-width: 768px) {.vc_custom_1540822934044{padding-top: 60px !important;padding-bottom: 60px !important;}}">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <h4 style="text-align: center"
                                class="ch_333870 vc_custom_heading vc_custom_1541319183512"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541319183512{margin-bottom: 15px !important;}} @media only screen and (max-width: 992px) {.ch_333870{}} @media only screen and (max-width: 768px) {.ch_333870{}} @media only screen and (max-width: 576px) {.ch_333870{}}">
                                CONTACT US</h4>
                            <h2 style="color: #ffffff;text-align: center"
                                class="ch_69101 vc_custom_heading vc_custom_1541319193864"
                                data-res-css=" @media only screen and (max-width: 768px) {.vc_custom_1541319193864{margin-bottom: 35px !important;}} @media only screen and (max-width: 992px) {.ch_69101{}} @media only screen and (max-width: 768px) {.ch_69101{}} @media only screen and (max-width: 576px) {.ch_69101{}}">
                                Get in Touch</h2>
                            <div class="wpcf7 no-js" id="wpcf7-f394-p279-o1" lang="en-US" dir="ltr">
                                <div class="screen-reader-response">
                                    <p role="status" aria-live="polite" aria-atomic="true"></p>
                                    <ul></ul>
                                </div>
                                <form action="/themes/cryptoland/#wpcf7-f394-p279-o1" method="post"
                                    class="wpcf7-form init demo" aria-label="Contact form" novalidate="novalidate"
                                    data-status="init">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="394" />
                                        <input type="hidden" name="_wpcf7_version" value="5.7.7" />
                                        <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f394-p279-o1" />
                                        <input type="hidden" name="_wpcf7_container_post" value="279" />
                                        <input type="hidden" name="_wpcf7_posted_data_hash" value="" />
                                    </div>
                                    <div class="form contact-form" id="contact-form">
                                        <div>
                                            <p><span class="wpcf7-form-control-wrap" data-name="text-669"><input
                                                        size="40"
                                                        class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form__input"
                                                        aria-required="true" aria-invalid="false"
                                                        placeholder="Name" value="" type="text"
                                                        name="text-669" /></span>
                                            </p>
                                        </div>
                                        <div>
                                            <p><span class="wpcf7-form-control-wrap" data-name="email-458"><input
                                                        size="40"
                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form__input"
                                                        aria-required="true" aria-invalid="false"
                                                        placeholder="Email" value="" type="email"
                                                        name="email-458" /></span>
                                            </p>
                                        </div>
                                        <div>
                                            <p><span class="wpcf7-form-control-wrap" data-name="textarea-850">
                                                    <textarea cols="40" rows="10"
                                                        class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form__textarea" aria-required="true"
                                                        aria-invalid="false" placeholder="Message" name="textarea-850"></textarea>
                                                </span>
                                            </p>
                                        </div>
                                        <div class="inline-block">
                                            <p><input
                                                    class="wpcf7-form-control has-spinner wpcf7-submit form__btn btn btn--big btn--white btn--orange"
                                                    type="submit" value="Send Message" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpcf7-response-output" aria-hidden="true"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
        <div class="container">
            <div id="custom_footer" data-vc-full-width="true" data-vc-full-width-init="false"
                class="ult-responsive vc_row wpb_row vc_row-fluid vc_custom_1540822995896 vc_row-o-content-middle vc_row-flex">
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper vc_custom_1540822206129"
                            data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540822206129{margin-top: 0px !important;margin-bottom: 0px !important;}}">
                            <a href="https://ninetheme.com/themes/cryptoland/" class="footer-logo logo"><img
                                    width="999" height="1144" src="{{ asset('images/isologo-blanco.png') }}"
                                    class="logo__img--big" alt="Logo_white.svg"
                                    data-lazy-src="{{ asset('images/isologo-blanco.png') }}"><noscript><img
                                        width="1144" height="1144"
                                        src="{{ asset('images/isologo-blanco.png') }}" class="logo__img--big"
                                        alt="Logo_white.svg"></noscript>
                                <div class="logo__title">Gosen</div>
                            </a>
                            <div style="font-size: 14px;color: #545c79;line-height: 25px;text-align: left"
                                class="ch_863336 f-catamaran vc_custom_heading vc_custom_1540103138302"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_863336{text-align:center!important;}} @media only screen and (max-width: 768px) {.ch_863336{text-align:left!important;}} @media only screen and (max-width: 576px) {.ch_863336{}}">
                                <a href="#0" title="About">© 2023, Gosen</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper vc_custom_1540103009667"
                            data-res-css=" @media only screen and (max-width: 992px) {.vc_custom_1540103009667{margin-bottom: 30px !important;}}">
                            <p style="font-size: 18px;color: #ffffff;line-height: 30px;text-align: left"
                                class="ch_686821 vc_custom_heading vc_custom_1540105361006"
                                data-res-css=" @media only screen and (max-width: 992px) {.ch_686821{text-align:center!important;}} @media only screen and (max-width: 768px) {.ch_686821{text-align:left!important;}} @media only screen and (max-width: 576px) {.ch_686821{}}">
                                Stay connected:</p>
                            <ul class="social-list">
                                <li class="social-list__item"><a href="#0" title="twitter"
                                        class="social-list__link"><i class="font-icon fa fa-twitter"></i></a></li>
                                <li class="social-list__item"><a href="#0" title="facebook"
                                        class="social-list__link"><i class="font-icon fa fa-facebook"></i></a></li>
                                <li class="social-list__item"><a href="#0" title="mail"
                                        class="social-list__link"><i class="font-icon fa fa-telegram"></i></a></li>
                                <li class="social-list__item"><a href="#0" title="Bitcoin"
                                        class="social-list__link"><i class="font-icon fa fa-btc"></i></a></li>
                                <li class="social-list__item"><a href="#" title="Youtube"
                                        class="social-list__link"><i class="font-icon fa fa-youtube-play"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-xs-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper ">
                            <div class="wpcf7 no-js" id="wpcf7-f395-p279-o2" lang="en-US" dir="ltr">
                                <div class="screen-reader-response">
                                    <p role="status" aria-live="polite" aria-atomic="true"></p>
                                    <ul></ul>
                                </div>
                                <form action="/themes/cryptoland/#wpcf7-f395-p279-o2" method="post"
                                    class="wpcf7-form init demo" aria-label="Contact form" novalidate="novalidate"
                                    data-status="init">
                                    <div style="display: none;">
                                        <input type="hidden" name="_wpcf7" value="395" />
                                        <input type="hidden" name="_wpcf7_version" value="5.7.7" />
                                        <input type="hidden" name="_wpcf7_locale" value="en_US" />
                                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f395-p279-o2" />
                                        <input type="hidden" name="_wpcf7_container_post" value="279" />
                                        <input type="hidden" name="_wpcf7_posted_data_hash" value="" />
                                    </div>
                                    <div class="form subscribe home3-form" id="subscribe-form">
                                        <div class="form__title">
                                            <p>Subscribe
                                            </p>
                                        </div>
                                        <div class="form__row">
                                            <p><span class="wpcf7-form-control-wrap" data-name="email-64"><input
                                                        size="40"
                                                        class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form__input"
                                                        aria-required="true" aria-invalid="false"
                                                        placeholder="Email" value="" type="email"
                                                        name="email-64" /></span><br />
                                                <input
                                                    class="wpcf7-form-control has-spinner wpcf7-submit form__btn btn btn--white btn--small btn--orange ml5"
                                                    type="submit" value="Send" />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="wpcf7-response-output" aria-hidden="true"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="vc_row-full-width vc_clearfix"></div>
        </div>
    </div>

    {{-- start includes resources --}}
    @include('includes.footer-resources')
    {{-- end includes resources --}}
</body>

</html>
